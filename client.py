#!/usr/bin/python
import requests
import argparse
import getpass

pswd = getpass.getpass('Password:')

url =  "http://52.221.111.51:9000"
headers = { 'Content-Type': 'application/json' }
def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument( "--hostname", default="dp.chronos.swiggyops.de:8081", metavar="<host:port>",
                         help="Host and port of instance where Chronos is hosted" )
    parser.add_argument( "--jobType", help="Schedule a job/Delete a job/Get status of a job/List all jobs. ( schedule, delete, status, list )" ) 
    parser.add_argument( "--name", help="Name of the Job", default="foo" )
    parser.add_argument( "--owner", help="Owner of Job. Use LDAP username", required=True )
    #parser.add_argument( "--password", help="Use LDAP password for authentication", required=True )
    parser.add_argument( "--file", help="Schedule Query. Write query in the file.", type=argparse.FileType('r') )
    parser.add_argument( "--timestamp", help="Time to schedule the job. Specify it in YYYY-MM-DDThh:mm:ss.sTZD format" )
    parser.add_argument( "--frequency", help="Frequency with which to run the job. Specify it as ISO 8601 standard." )
    parser.add_argument( "--parent", default=None, help="Parent of the job." )
    args = parser.parse_args()
    return args

def run():
    args = parse()

    # Authorize LDAP credentials.
    owner = args.owner
    password = pswd
    jobName = args.name
    host = args.hostname
    
    if args.jobType == "delete":
        data = { "host": host, "jobName": jobName, "owner": owner, "password": password }
        results = requests.post( url + "/delete", headers=headers, params=data )
        
        if results.text == 'LDAP Authorization Failed':
            print 'LDAP Authorization Failed'
        elif results.text == 'False owner':
            print "Not authorized to delete someone else's query"
        elif results.text == 'MYSQL Failure':
            print "Query couldn't be deleted. MYSQL Exception."
        elif results.text == 'Fail':
            print "Query deletion failed."
        elif results.status_code == 200:
            print "Query deleted"
    elif args.jobType == "schedule":
        jobCommand = args.file
        timestamp = args.timestamp
        frequency = args.frequency
        cmd = jobCommand.read()

        if args.parent:
            data = { "host": host, "jobName": jobName, "owner": owner, "cmd": cmd, "parent": args.parent, "dependent_job": "YES", "password": password }
        else:
            schedule = "R/" + timestamp + "Z/" + frequency
            data = { "host": host, "jobName": jobName, "owner": owner, "cmd": cmd, "schedule": schedule, "dependent_job": "NO", "password": password }
        results = requests.post( url + "/schedule", headers=headers, params=data )

        if results.text == 'LDAP Authorization Failed':
            print 'LDAP Authorization Failed'
        elif results.text == 'MYSQL Failure':
            print "Query couldn't be scheduled. MYSQL Exception."
        elif results.text == 'Fail':
            print "Query scheduling failed."
        elif results.text == 'Parent error':
            print "Parent job is not present"
        elif results.status_code == 200:
            print "Query is Scheduled"
    elif args.jobType == "status":
        api_url = url + "/status"
        data = { "jobName": jobName }
        results = requests.post( api_url, headers=headers, params=data )
        print results.text
    elif args.jobType == "list":
        api_url = url + "/list"
        data = { "owner": owner, "password": password }
        results = requests.post( api_url, headers=headers, params=data )
        print results.text

if __name__ == '__main__':
    run()

