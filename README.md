Requirements - Python2.7 and pip
Python modules required for scheduling query can be installed by running - 
"""pip install -r requirements.txt"""

python client.py --help
usage: client.py [-h] [--hostname <host:port>] [--jobType JOBTYPE]
                 [--name NAME] --owner OWNER --password PASSWORD [--file FILE]
                 [--timestamp TIMESTAMP] [--frequency FREQUENCY]
                 [--parent PARENT]

optional arguments:
  -h, --help            show this help message and exit
  --hostname <host:port>
                        Host and port of instance where Chronos is hosted
  --jobType JOBTYPE     Schedule a job/Delete a job/Get status of a job/List
                        all jobs. ( schedule, delete, status, list )
  --name NAME           Name of the Job
  --owner OWNER         Owner of Job. Use LDAP username
  --password PASSWORD   Use LDAP password for authentication
  --file FILE           Schedule Query. Write query in the file.
  --timestamp TIMESTAMP
                        Time to schedule the job. Specify it in YYYY-MM-
                        DDThh:mm:ss.sTZD format
  --frequency FREQUENCY
                        Frequency with which to run the job. Specify it as ISO
                        8601 standard.
  --parent PARENT       Parent of the job.


"""Scheduling a Query"""
python client.py --jobType schedule --file test_query --timestamp 2018-05-03T07:25:00 --frequency PT45M --name "select_test" --owner "sidharth.singla@swiggy.in" --password "xyz"
python client.py --jobType schedule --file test_query --parent "select_test" --name "select_test_2" --owner "sidharth.singla@swiggy.in" --password "xyz"

"""Deleting a Query"""
python client.py --jobType delete --name "select_test" --owner "sidharth.singla@swiggy.in" --password "xyz"

"""Status of Query"""
python client.py --jobType status --name "GTM_Count_Compare_2" --owner "sidharth.singla@swiggy.in" --password "xyz"

{'description': u'Compares GTM with last week same time data', 'errorsSinceLastSuccess': 0, 'successCount': 112, 'parents': [u'OnboardQuery_Docker'], 'lastSuccess': u'2018-05-07T05:28:53.952Z', 'owner': u'data-platform@swiggy.in, dponcall-data@swiggy.pagerduty.com', 'lastError': u'', 'errorCount': 0, 'name': u'GTM_Count_Compare_2'}

"""Listing all jobs of owner"""
python client.py --jobType list --owner "sidharth.singla@swiggy.in" --password "xyz"

[{u'softError': False, u'scheduleTimeZone': u'', u'successCount': 165, u'cpus': 0.1, u'disabled': False, u'ownerName': u'', u'owner': u'sidharth.singla@swiggy.in', u'disk': 256.0, u'errorCount': 0, u'container': {u'network': u'HOST', u'parameters': [], u'image': u'shr-p-nexus-02.swiggyops.de:8083/adpg:1', u'forcePullImage': False, u'volumes': [], u'networkInfos': [], u'type': u'DOCKER'}, u'errorsSinceLastSuccess': 0, u'highPriority': False, u'dataProcessingJobType': False, u'arguments': [], u'uris': [], u'shell': True, u'description': u'', u'schedule': u'R/2018-05-09T12:40:00.000Z/PT45M', u'mem': 600.0, u'concurrent': False, u'taskInfoData': u'', u'retries': 2, u'name': u'select_test', u'runAsUser': u'root', u'lastSuccess': u'2018-05-09T11:55:18.537Z', u'environmentVariables': [], u'executorFlags': u'', u'command': u'java -jar ./home/facts.jar custom-query name::select_test', u'executor': u'', u'lastError': u'', u'fetch': [], u'constraints': []}]