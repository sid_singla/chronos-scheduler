import MySQLdb
import httplib, requests, json
from flask import request, Flask
import ldap

app = Flask(__name__)

host = "localhost:8081"

def dbConnect():
    db = MySQLdb.connect( "data-platform-configs.ckvce9fjaook.ap-southeast-1.rds.amazonaws.com", "root", "Y6AhkfpYedbcf8Us", "dp_management" )
    db.autocommit = True
    cursor = db.cursor()
    cursor.autocommit = True
    return db, cursor

def authorize( owner, password ):
    ldap_server = "ldap://awsad.bundl.in"
    username = owner.replace( "swiggy.in", "bundl.in" )

    try:
        ldap_client = ldap.initialize( ldap_server )
        res = ldap_client.bind_s( username, password )
        return "Success"
    except:
        return "Fail"
        
@app.route('/schedule', methods=['POST'])
def schedule():
    try:
        owner = request.args.get( 'owner' )        
        password = request.args.get( 'password' )
        if authorize( owner, password ) == 'Fail':
            return 'LDAP Authorization Failed'
        cmd = request.args.get( 'cmd' )
        cmd = cmd.strip('\n')
        cmd = cmd.strip('\t')
        cmd = cmd.strip('\r')
        if cmd[-1] == ';':
            cmd = cmd[:-1]
        jobName = request.args.get( 'jobName' )
        
        isJobDependent = request.args.get( 'dependent_job' )
        
        cmds = cmd.split(";")
        queries = []
  
        for command in cmds:
            query = {}
            query[ "type" ] = "INSERT"
            query[ "query" ] = command
            queries.append(query)

        db, cursor = dbConnect()
        insert_query = """INSERT INTO custom_queries ( name, queries, created_at ) VALUES( %s, %s, NOW() )"""
        cursor.execute( insert_query, ( jobName, json.dumps(queries) ) )
        db.commit()
        cursor.close()
        db.close()

        headers = { 'Content-Type': 'application/json' }
        connection = httplib.HTTPConnection( host )

        if isJobDependent == 'YES':            
            parent = request.args.get( 'parent' )
            res = getJobData( parent )
            if not res.json():
                return "Parent error"
 
            data = '{"parents":["%s"],"name":"%s","command":"java -jar ./home/facts.jar custom-query name::%s","owner":"%s","async":false,"container":{"type":"DOCKER","image":"shr-p-nexus-02.swiggyops.de:8083/adpg:1"},"mem":600,"mail_from":"alerts.dp@swiggy.in","mail_password":"pzblwrqbkrxcrnqj","mail_server":"smtp.gmail.com:587","mail_user":"alerts.dp@swiggy.in"}' % ( parent, jobName, jobName, owner )
            connection.request("POST", "/v1/scheduler/dependency", data, headers)
        else:
            schedule = request.args.get( 'schedule' )
            data = '{"schedule":"%s","name":"%s","command":"java -jar ./home/facts.jar custom-query name::%s","owner":"%s","async":false,"container":{"type":"DOCKER","image":"shr-p-nexus-02.swiggyops.de:8083/adpg:1"},"mem":600,"mail_from":"alerts.dp@swiggy.in","mail_password":"pzblwrqbkrxcrnqj","mail_server":"smtp.gmail.com:587","mail_user":"alerts.dp@swiggy.in"}' % ( schedule, jobName, jobName, owner )
            connection.request("POST", "/v1/scheduler/iso8601", data, headers)

        connection.getresponse().read()
        connection.close()

        return 'Success'
    except MySQLdb.Error, e:
        print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
        return 'MYSQL Failure'
    except:
        return 'Fail'

def getJobData( jobName ):
    job_data = { 'name': jobName }
    res = requests.get( "http://localhost:8081/v1/scheduler/jobs/search", params=job_data  )
    return res

@app.route('/status', methods=['POST'])
def status():
    jobName = request.args.get( 'jobName' )
    job_data = getJobData( jobName ).json()[ 0 ]
    jobStatus = {}
    jobStatus[ 'owner' ] = job_data[ 'owner' ]
    jobStatus[ 'errorCount' ] = job_data[ 'errorCount' ]
    jobStatus[ 'successCount' ] = job_data[ 'successCount' ]
    jobStatus[ 'errorsSinceLastSuccess' ] = job_data[ 'errorsSinceLastSuccess' ]
    if 'parents' in job_data.keys():
        jobStatus[ 'parents' ] = job_data[ 'parents' ]
    jobStatus[ 'description' ] = job_data[ 'description' ]
    jobStatus[ 'name' ] = job_data[ 'name' ]
    jobStatus[ 'lastSuccess' ] = job_data[ 'lastSuccess' ]
    jobStatus[ 'lastError' ] = job_data[ 'lastError' ]
    return str( jobStatus )
    
@app.route('/delete', methods=['POST'])
def deleteJob():
    try:
        owner = request.args.get( 'owner' )
        password = request.args.get( 'password' )
        if authorize( owner, password ) == 'Fail':
            return 'LDAP Authorization Failed'

        jobName = request.args.get( 'jobName' )
        job_data = getJobData( jobName ).json()[ 0 ]
        job_owners = job_data[ 'owner' ].split( ',' )
        if owner not in job_owners:
            return 'False owner'

        db, cursor = dbConnect()
        delete_query = """DELETE FROM custom_queries WHERE name='%s'""" % jobName
        cursor.execute( delete_query )
        db.commit()
        cursor.close()
        db.close()

        connection = httplib.HTTPConnection( host )
        connection.request( "DELETE", "/v1/scheduler/job/%s" % jobName )
        connection.getresponse().read()
        connection.close()
        return 'Success'
    except MySQLdb.Error, e:
        print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
        return 'MYSQL Failure'
    except:
        return 'Fail'

@app.route('/list', methods=['POST'])
def getJobs():
    owner = request.args.get( 'owner' )
    password = request.args.get( 'password' )

    if authorize( owner, password ) == 'Fail':
        return 'LDAP Authorization Failed'

    res = requests.get( "http://localhost:8081/v1/scheduler/jobs" )
    jobs = res.json()
    owner_jobs = []
    for job in jobs:
        job_owners = job[ 'owner' ].split( ',' )
        if owner in job_owners:
            owner_jobs.append( job )

    return str( owner_jobs )

if __name__ == '__main__':
    app.run( host="0.0.0.0", port=9000 )
